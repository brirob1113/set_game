require './lib/board.rb'

RSpec.describe Board do

  let(:board){Board.new []}

  describe "#find_set" do

    let(:card1){Card.new(:red, :diamond, :solid, :one)}
    let(:card2){Card.new(:red, :oval, :solid, :one)}
    let(:card3){Card.new(:purple, :diamond, :empty, :two)}
    let(:card4){Card.new(:green, :squiggle, :striped, :three)}
    let(:card5){Card.new(:green, :squiggle, :striped, :three)}
    let(:card6){Card.new(:green, :squiggle, :striped, :three)}

    it 'returns nil if no set' do
      expect(board.find_set).to be_nil
    end

    it 'returns first three cards that form a set' do
      board.cards = [card1, card2, card3, card4, card5, card6]
      expect(board.find_set).to eq([card2, card3, card4])
    end

    it 'removes cards from board if set found' do
      board.cards = [card1, card2, card3, card4, card5, card6]
      board.find_set
      expect(board.cards).to eq([card1, card5, card6])
    end
  end

  describe "#add_cards" do
    let(:new_cards){[1,2,3]}
    it 'pushes new cards to board' do
      board.cards = [4,5,6]
      board.add_cards new_cards
      expect(board.cards).to eq([4,5,6,1,2,3])
    end
  end
end