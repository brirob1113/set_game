require './lib/card.rb'

RSpec.describe Card do
  it{ expect(Card.new(:red, :diamond, :solid, :one)).to have_attributes(color: :red, shape: :diamond, shading: :solid, number: :one)}
end