require './lib/deck.rb'


RSpec.describe Deck do

  let(:deck){Deck.new}

  describe "#shuffle" do

    it 'creates a deck of 81 cards' do
      deck.shuffle
      expect(deck.cards.size).to eq(81)
    end
  end

  describe "#deal" do
    let(:cards){(1..20).to_a}

    before(:each){ deck.cards = cards }

    it 'returns 12 random cards' do
      expect(deck.deal.size).to eq(12)
    end

    it 'removes dealt cards' do
      deck.deal
      expect(deck.cards.size).to eq(8)
    end
  end

  describe "#draw" do
    let(:cards){(1..5).to_a}

    before(:each){ deck.cards = cards }

    it 'returns 3 random cards' do
      expect(deck.draw.size).to eq(3)
    end

    it 'removes drawn cards from deck' do
      deck.draw
      expect(deck.cards.size).to eq(2)
    end
  end

  describe "#empty" do

    it 'returns true if cards are empty' do
      deck.cards = []
      expect(deck.empty?).to be true
    end

    it 'returns true if cards are empty' do
      deck.cards = [1,2,3]
      expect(deck.empty?).to be false
    end

  end
end