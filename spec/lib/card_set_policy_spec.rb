require './lib/card_set_policy.rb'

RSpec.describe CardSetPolicy do
  describe "#set?" do

    let(:card1){Card.new(:red, :oval, :solid, :one)}
    let(:card2){Card.new(:red, :oval, :solid, :one)}
    let(:card3){Card.new(:red, :oval, :solid, :one)}

    let(:card4){Card.new(:purple, :diamond, :empty, :two)}
    let(:card5){Card.new(:green, :squiggle, :striped, :three)}

    let(:card6){Card.new(:purple, :oval, :solid, :one)}
    let(:card7){Card.new(:red, :diamond, :solid, :one)}
    let(:card8){Card.new(:red, :oval, :empty, :one)}
    let(:card9){Card.new(:red, :oval, :solid, :two)}

    it "returns true if all cards attributes match" do
      policy = CardSetPolicy.new card1, card2, card3
      expect(policy.set?).to be true
    end

    it "returns true if all cards attributes dont match" do
      policy = CardSetPolicy.new card1, card4, card5
      expect(policy.set?).to be true
    end

    it "returns false if not all color attributes are the same or different" do
      policy = CardSetPolicy.new card1, card2, card6
      expect(policy.set?).to be false
    end

    it "returns false if not all shape attributes are the same or different" do
      policy = CardSetPolicy.new card1, card2, card7
      expect(policy.set?).to be false
    end

    it "returns false if not all shading attributes are the same or different" do
      policy = CardSetPolicy.new card1, card2, card8
      expect(policy.set?).to be false
    end

    it "returns false if not all number attributes are the same or different" do
      policy = CardSetPolicy.new card1, card2, card9
      expect(policy.set?).to be false
    end
  end
end