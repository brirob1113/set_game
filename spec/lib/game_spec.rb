require './lib/game.rb'
require './lib/deck.rb'

RSpec.describe Game do

  let(:game){Game.new}
  let(:deck){double(:deck)}
  let(:board){double(:board)}

  it{ expect(Game.new).to have_attributes(sets: [])}

  describe "#setup" do
    let(:dealt_cards){['some', 'cards']}

    before :each do
      allow(Deck).to receive(:new).and_return(deck)
      allow(deck).to receive(:shuffle)
      allow(deck).to receive(:deal).and_return(dealt_cards)
      allow(Board).to receive(:new).with(dealt_cards).and_return(board)
    end

    it 'sets up deck' do
      game.setup
      expect(game.deck).to eq(deck)
    end

    it 'shuffles deck' do
      expect(deck).to receive(:shuffle)
      game.setup
    end

    it 'sets up board' do
      game.setup
      expect(game.board).to eq(board)
    end

    it 'reinitialize result set' do
      game.setup
      expect(game.sets).to eq([])
    end

  end

  describe '#play' do

    before(:each){ game.board = board }

    it 'handles no set when cant find a set' do
      allow(board).to receive(:find_set).and_return(nil)
      expect(game).to receive(:no_set)
      game.play
    end

    it 'handles has set when found a set' do
      game.board = board
      allow(board).to receive(:find_set).and_return(['stuff'])
      expect(game).to receive(:has_set)
      game.play
    end
  end

  describe '#has_set' do

    before :each do
      game.deck = deck
      game.board = board
      allow(game).to receive(:play)
      allow(deck).to receive(:empty?).and_return(true)
    end

    it 'adds new set to saved sets' do
      game.has_set [1,2,3]
      expect(game.sets).to eq([[1,2,3]])
    end

    it 'game continues to play' do
      expect(game).to receive(:play)
      game.has_set([])
    end

    it 'adds cards to board if deck is not empty' do
      new_cards = [1,2,3]
      allow(deck).to receive(:draw).and_return(new_cards)
      allow(deck).to receive(:empty?).and_return(false)

      expect(board).to receive(:add_cards).with(new_cards)
      game.has_set []
    end
  end

  describe '#no_set' do

    let(:new_cards){[1,2,3]}

    before :each do
      game.deck = deck
      game.board = board
      allow(deck).to receive(:empty?).and_return(false)
      allow(deck).to receive(:draw).and_return(new_cards)
      allow(board).to receive(:add_cards).with(new_cards)
      allow(game).to receive(:play)
    end

    it 'continues play if deck is not empty' do
      expect(game).to receive(:play)
      game.no_set
    end

    it 'deals cards if deck is not empty' do
      expect(board).to receive(:add_cards).with(new_cards)
      game.no_set
    end

    it 'stops play if deck is empty' do
      allow(deck).to receive(:empty?).and_return(true)
      expect(game).not_to receive(:play)
      game.no_set
    end
  end

end