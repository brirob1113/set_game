require 'awesome_print'

class Deck

  attr_accessor :cards

  COLORS  = %i(red green purple)
  SHAPES  = %i(diamond squiggle oval)
  SHADING = %i(solid empty striped)
  NUMBERS = %i(one two three)

  def shuffle
    self.cards = []
    COLORS.each do |color|
      SHAPES.each do |shape|
        SHADING.each do |shade|
          NUMBERS.each do |number|
            cards.insert random_index, Card.new(color, shape, shade, number)
          end
        end
      end
    end
  end

  def deal
    select_random_cards(12)
  end

  def draw
    select_random_cards(3)
  end

  def empty?
    cards.empty?
  end

  private

  def select_random_cards amount
    (1..amount).map do |i|
      cards.delete_at(rand(cards.size))
    end
  end

  def random_index
    rand(cards.size + 1)
  end
end