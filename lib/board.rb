class Board

  attr_accessor :cards

  def initialize cards
    self.cards = cards
  end

  def find_set
    if cards.size >= 3
      for i in (0..cards.size-1)
        for j in (i+1..cards.size-1)
          for k in (j+1..cards.size-1)
            return matching_set(i, j, k) if CardSetPolicy.new(cards[i], cards[j], cards[k]).set?
          end
        end
      end
    end
    return nil
  end

  def add_cards new_cards
    self.cards = self.cards + new_cards
  end

  private

  def matching_set(i, j, k)
    [cards.delete_at(i), cards.delete_at(j-1), cards.delete_at(k-2)]
  end

end