class Game
  attr_accessor :deck, :board, :sets

  def initialize
    initialize_sets
  end

  def setup
    initialize_sets
    self.deck = Deck.new
    deck.shuffle
    self.board = Board.new(deck.deal)
  end

  def play
    set = board.find_set
    set == nil ? no_set : has_set(set)
  end

  def has_set(set)
    sets << set
    board.add_cards deck.draw unless deck.empty?
    play
  end

  def no_set
    unless deck.empty?
      board.add_cards deck.draw
      play
    end
  end

  def print_results
    ap '*'*40
    ap "total sets found: #{sets.size}"
    sets.each do |set|
      ap set
    end

    ap "cards left: #{board.cards.size}"
    board.cards.each do |card|
      ap card
    end
  end

  private

  def initialize_sets
    self.sets = []
  end

end