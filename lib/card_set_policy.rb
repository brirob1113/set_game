class CardSetPolicy

  def initialize card1, card2, card3
    @card1, @card2, @card3 = card1, card2, card3
  end

  def set?
    return all_match_or_mismatch?(:color) &&
           all_match_or_mismatch?(:shape) &&
           all_match_or_mismatch?(:shading) &&
           all_match_or_mismatch?(:number)
  end

  private

  def all_match_or_mismatch? attribute
    all_match(attribute) || all_mismatch(attribute)
  end

  def all_mismatch(attribute)
    @card1.send(attribute) != @card2.send(attribute) &&
        @card1.send(attribute) != @card3.send(attribute) &&
        @card2.send(attribute) != @card3.send(attribute)
  end

  def all_match(attribute)
    @card1.send(attribute) == @card2.send(attribute) &&
        @card1.send(attribute) == @card3.send(attribute)
  end

end