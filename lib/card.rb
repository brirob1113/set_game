class Card

  attr_accessor :color, :shape, :shading, :number

  def initialize color, shape, shading, number
    self.color, self.shape, self.shading, self.number  = color, shape, shading, number
  end

  def inspect
    "#{color},\t#{shape},   \t#{shading},  \t#{number}"
  end

end